# localConfig

reads local config from a JSON file

example:

```golang
package main

import (
    "fmt"
    "flag"
    "localconfig"
)

type localConfig struct {
	Server struct {
		Port int    `json:"port" envconfig:"SERVER_PORT"`
		IP   string `json:"ip" envconfig:"SERVER_IP"`
		Host string `json:"host" envconfig:"SERVER_HOST"`
		SSL  bool   `json:"ssl" envconfig:"SERVER_SSL"`
	} `json:"server"`
	Database struct {
		Host string `json:"host" envconfig:"DB_HOST"`
		User string `json:"user" envconfig:"DB_USER"`
		Pass string `json:"pass" envconfig:"DB_PASS"`
		Name string `json:"name" envconfig:"DB_NAME"`
	} `json:"database"`
}

var conf localConfig
var configFile = flag.String("configFile", "config.json", "path to the config file")

func init() {

	flag.Parse()

	// Read from file first.
	err := localconfig.ReadFromFile(&conf, *configFile)
	// check err
	// Then read from ENV to override config file
	err = localconfig.ReadFromEnv(&conf)
	// check err
}

func main() {
    fmt.Printf("Database Host: %s", conf.Database.Host)
}
```

Config file should look like:

```javascript
{
	"server": {
		"port": 8080,
		"ip": "127.0.0.1",
		"host": "localhost",
		"ssl": true
	},
	"database": {
		"host": "localhost:54320",
		"user": "user",
		"pass": "set via DB_PASS env variable",
		"name": "blah"
	}
}
```
