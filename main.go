package localconfig

import (
	"embed"
	"encoding/json"
	"io/ioutil"

	"github.com/kelseyhightower/envconfig"
)

// ReadFromFile reads a file and tries to parse it into configStruct
func ReadFromFile(configStruct interface{}, configFile string) (err error) {
	bconf, err := ioutil.ReadFile(configFile)
	if err != nil {
		return
	}

	err = json.Unmarshal(bconf, configStruct)
	return

}

// ReadFromEmbeddedFile reads a file and tries to parse it into configStruct
func ReadFromEmbeddedFile(efs *embed.FS, configStruct interface{}, configFile string) (err error) {
	bconf, err := efs.ReadFile(configFile)
	if err != nil {
		return
	}

	err = json.Unmarshal(bconf, configStruct)
	return

}

// ReadFromEnv reads defined environment variables and tries to parse it into configStruct
func ReadFromEnv(cfg interface{}) (err error) {
	err = envconfig.Process("", cfg)
	return
}
